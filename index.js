//load the expressjs module into our application and save it in a variable called express.
const express = require("express");

//localhost port number
const port = 4000;

//app is our server
//create an application that uses and stores it as app.
const app = express();


//middleware
//express.json() is a method which allow us to handel the streaming of data and automatically parse the incoming json from our request. - para automatic naka json format - global
app.use(express.json());

//mockdatabase
let users = [
		{
			username: "TStark3000",
			email: "starkindustries@mail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "loveandthunder@mail.com",
			password: "iLoveStormBreaker"
		}
	]

	//express has methods to use as routes corresponding to HTTP methods
	//Syntax:
		//app.method(<endpoint>, function for request and response)

	//[HTTP method GET]
		app.get("/", (request, response) => {
			//response.status = writeHead
			//response.send = write with end()
			response.status(201).send("Hello from express!")
		})

	//Mini Activity:
		//create a "get" route in expressjs which will be able to send a message in the client:
		//endpoint: /greeting
		//message: "Hello from Batch245-surname"
		//status code : 201

		app.get("/greeting", (request, response) => {
			response.status(201).send("Hello from Batch245-manalang!")
		})

		app.get("/users", (request, response) => {
		
			response.send(users);
		})

	//[HTTP method for POST]
		app.post("/users", (request, response) => {
			let input = request.body;

			let newUser = {
				username: input.username,
				email: input.email,
				password: input.password
			}

			users.push(newUser)

			response.send(`The ${newUser.username} is now registered on our website with email: ${newUser.email}!`);
		})

	//[HTTP method for DELETE]
		app.delete("/users", (request, response) => {
			
			let deletedUser = users.pop();

			response.send(deletedUser);
		})

	//[HTTP method for PUT]
		app.put("/users/:index", (request, response) => {
			let indexToBeUpdated = request.params.index
			console.log(typeof indexToBeUpdated);

			indexToBeUpdated = parseInt(
				indexToBeUpdated);

			if(indexToBeUpdated < uses.length)
			{
				users[indexToBeUpdated].password = request.body.password;

			response.send(users[indexToBeUpdated]);
		
			}else{
				response.status(404)
			} 	

			})

app.listen(port, ()=> console.log("Server is running at port 4000"))